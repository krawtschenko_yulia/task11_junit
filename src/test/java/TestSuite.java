import org.junit.jupiter.api.DisplayName;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;

@DisplayName("Test Suite")
@RunWith(JUnitPlatform.class)
@SelectPackages({
        "me.krawtschenko.yulia.tests.ApplicationTest",
        "me.krawtschenko.yulia.tests.LongestPlateauTest",
        "me.krawtschenko.yulia.tests.MinesweeperTest"
})
public class TestSuite {
    Result result = JUnitCore.runClasses();
}
