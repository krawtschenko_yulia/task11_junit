package me.krawtschenko.yulia.tests;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import me.krawtschenko.yulia.Minesweeper;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MinesweeperTest {
    int m = 5;
    int n = 6;
    double mineProbability = 0.4;
    private final Minesweeper minesweeper = new Minesweeper(
            m, n, mineProbability
    );

    @Test
    void assertCreated() {
        assertNotNull(minesweeper);
        assertNotNull(minesweeper.getPrintableField());
        assertNotNull(minesweeper.getInfoField());
    }

    @Disabled
    @Test
    void testFieldGeneration() throws NoSuchFieldException, IllegalAccessException {

        Field field = Minesweeper.class.getDeclaredField("mine");
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, false);

        Minesweeper flipped = new Minesweeper(m, n, mineProbability);
        assertNotNull(flipped);
    }

    @RepeatedTest(10)
    void testRandomization() {
        assertNotSame(minesweeper, new Minesweeper(
                m, n, mineProbability
        ));
    }
}
