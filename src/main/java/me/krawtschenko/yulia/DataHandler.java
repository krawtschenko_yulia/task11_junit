package me.krawtschenko.yulia;

public class DataHandler {
    DataReader reader = new DataReader();
    DataWriter writer = new DataWriter();

    public boolean copy(String inputFilename, String outputFilename) {
        String data = reader.read(inputFilename);
        writer.write(data, outputFilename);

        return true;
    }
}
